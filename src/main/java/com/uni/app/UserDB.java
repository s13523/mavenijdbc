package com.uni.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDB {
	public UserDB() {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

            provideTablesInDB();
            initPreparedStatements();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addUser(User user) {
		try {
			addUserStatement.setString(1, user.getLogin());
			addUserStatement.setString(2, user.getPass());
            addUserStatement.setString(3, user.getPhone());

			addUserStatement.executeUpdate();
            int userid = getLastID(addUserStatement);

            if( user.getAddresses().size() > 0 ){
                for( String address : user.getAddresses() ){
                    addAddressStatement.setString(1, address);
                    addAddressStatement.executeUpdate();
                    int addressid = getLastID(addAddressStatement);
                    addUserAddressStatement.setInt(1, userid);
                    addUserAddressStatement.setInt(2, addressid);
                    addUserAddressStatement.executeUpdate();
                }
            }

            if( user.getRoles().size() > 0 ){
                for( Role role : user.getRoles() )
                    linkRole( userid, role );
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

    public void clearUsers() {
        try{
            clearAddressesStatement.executeUpdate();
            clearUserAddressesStatement.executeUpdate();
            clearUsersStatement.executeUpdate();
        } catch( SQLException e ){
            e.printStackTrace();
        }
    }

	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();

		try {
			ResultSet results = getUsersStatement.executeQuery();

			while (results.next()) {
                User newUser = new User(
                    results.getString( "login" ),
                    results.getString( "pass" ),
                    results.getString( "phone" ),
                    null,
                    null
                );

                int userid = results.getInt( "id" );
                results.close();

                getUserAddressStatement.setInt( 1, userid );
                results = getUserAddressStatement.executeQuery();

                while( results.next() ){
                    newUser.withAddress( results.getString( "content" ) );
                }

                getUserRoleStatement.setInt( 1, userid );
                results = getUserRoleStatement.executeQuery();
                while( results.next() ){
                    Role role = new Role( results.getString( "name" ), null );
                    getRolePermissionsStatement.setInt( 1, results.getInt( "id" ) );
                    ResultSet perms = getRolePermissionsStatement.executeQuery();
                    while( perms.next() ){
                        role.withPermission( perms.getString( "name" ) );
                    }
                    newUser.withRole( role );   
                }

				users.add( newUser );
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
    
    private void linkRole( int userid, Role role ) throws SQLException{
        linkRoleStatement.setInt( 1, userid );
        linkRoleStatement.setInt( 2, getRoleID( role ) );
        linkRoleStatement.executeUpdate();
    }

    private int getRoleID( Role role ) throws SQLException{
        findRoleStatement.setString( 1, role.getName() );
        ResultSet rolesFound = findRoleStatement.executeQuery();
        if( rolesFound.next() )
            return rolesFound.getInt("id");

        return getNewRoleID( role );
    }

    private int getNewRoleID( Role role ) throws SQLException{
        newRoleStatement.setString( 1, role.getName() );
        newRoleStatement.executeUpdate();
        int roleid = getLastID( newRoleStatement );

        for( String perm : role.getPermissions() ){
            linkPermission( perm, roleid );
        }

        return roleid;
    }

    private void linkPermission( String permission, int roleid ) throws SQLException{
        findPermissionStatement.setString( 1, permission );
        ResultSet found = findPermissionStatement.executeQuery();
        if( found.next() )
            linkPermissionStatement.setInt( 1, found.getInt( "id" ) );
        else{
            addPermissionStatement.setString( 1, permission );
            addPermissionStatement.executeUpdate();
            linkPermissionStatement.setInt( 1,
                getLastID( addPermissionStatement )
            );
        }

        linkPermissionStatement.setInt( 2, roleid );
        linkPermissionStatement.executeUpdate();
    }

    private void initPreparedStatements() throws SQLException{
        addUserStatement = connection.prepareStatement(
            "INSERT INTO User (id, login, pass, phone) VALUES (NULL, ?, ?, ?)",
            Statement.RETURN_GENERATED_KEYS
        );
        addAddressStatement = connection.prepareStatement(
            "INSERT INTO Address (content) VALUES (?)",
            Statement.RETURN_GENERATED_KEYS
        );
        addUserAddressStatement = connection.prepareStatement(
            "INSERT INTO UserAddress (userid, addressid) VALUES (?, ?)",
            Statement.RETURN_GENERATED_KEYS
        );
        getUsersStatement = connection.prepareStatement(
            "SELECT id, login, pass, phone FROM User"
        );
        getUserAddressStatement = connection.prepareStatement(
            "SELECT content FROM Address,UserAddress WHERE addressid=Address.id AND userid=?"
        );

        clearUsersStatement = connection.prepareStatement(
            "DELETE FROM User"
        );
        clearUserAddressesStatement = connection.prepareStatement(
            "DELETE FROM UserAddress"
        );
        clearAddressesStatement = connection.prepareStatement(
            "DELETE FROM Address"
        );
        findRoleStatement = connection.prepareStatement(
            "SELECT id FROM Role WHERE name=?"
        );
        newRoleStatement = connection.prepareStatement(
            "INSERT INTO Role (name) VALUES (?)",
            Statement.RETURN_GENERATED_KEYS
        );
        findPermissionStatement = connection.prepareStatement(
            "SELECT id FROM Permission WHERE name=?;"
        );
        addPermissionStatement = connection.prepareStatement(
            "INSERT INTO Permission (name) VALUES (?)",
            Statement.RETURN_GENERATED_KEYS
        );
        linkRoleStatement = connection.prepareStatement(
            "INSERT INTO UserRole (userid, roleid) VALUES (?, ?)"
        );
        linkPermissionStatement = connection.prepareStatement(
            "INSERT INTO RolePermission (permid, roleid) VALUES (?, ?)"
        );
        getUserRoleStatement = connection.prepareStatement(
            "SELECT id, name FROM Role, UserRole WHERE roleid=id AND userid=?"
        );
        getRolePermissionsStatement = connection.prepareStatement(
            "SELECT id, name FROM Permission, RolePermission WHERE id=permid AND roleid=?"
        );
    }

    private void provideTablesInDB() throws SQLException{
        if( !tableExists( "User" ) )
            statement.executeUpdate(
                "CREATE TABLE User("
                +"id bigint GENERATED BY DEFAULT AS IDENTITY,"
                +"login varchar(20), pass varchar(20),"
                +"phone varchar(20))"
            );
        if( !tableExists( "Address" ) )
            statement.executeUpdate(
                "CREATE TABLE Address("
                +"id bigint GENERATED BY DEFAULT AS IDENTITY,"
                +"content varchar(512))"
            );
        if( !tableExists( "UserAddress" ) )
            statement.executeUpdate(
                "CREATE TABLE UserAddress("
                +"userid bigint,"
                +"addressid bigint)"
            );
        if( !tableExists( "Role" ) )
            statement.executeUpdate(
                "CREATE TABLE Role("
                +"id bigint GENERATED BY DEFAULT AS IDENTITY,"
                +"name varchar(20))"
            );
        if( !tableExists( "UserRole" ) )
            statement.executeUpdate(
                "CREATE TABLE UserRole("
                +"userid bigint,"
                +"roleid bigint)"
            );
        if( !tableExists( "Permission" ) )
            statement.executeUpdate(
                "CREATE TABLE Permission("
                +"id bigint GENERATED BY DEFAULT AS IDENTITY,"
                +"name varchar(20))"
            );
        if( !tableExists( "RolePermission" ) )
            statement.executeUpdate(
                "CREATE TABLE RolePermission("
                +"roleid bigint,"
                +"permid bigint)"
            );
    }

    private boolean tableExists( String name ) throws SQLException{
	    ResultSet tables = connection.getMetaData().getTables(
            null, null, null, null
        );

        boolean result = false;
        while (tables.next()) {
            if (name.equalsIgnoreCase(tables.getString("TABLE_NAME"))) {
                result = true;
                break;
            }
        }
        return result;
    }

    private int getLastID( PreparedStatement statement ) throws SQLException{
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getInt(1);
    }

	private Connection connection;
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";

	private PreparedStatement addUserStatement;
    private PreparedStatement addAddressStatement;
    private PreparedStatement addUserAddressStatement;
    private PreparedStatement getLastUserStatement;
    private PreparedStatement getLastAddressStatement;
	private PreparedStatement getUsersStatement;
    private PreparedStatement getUserAddressStatement;
    private PreparedStatement clearUsersStatement;
    private PreparedStatement clearUserAddressesStatement;
    private PreparedStatement clearAddressesStatement;
    private PreparedStatement findRoleStatement;
    private PreparedStatement newRoleStatement;
    private PreparedStatement findPermissionStatement;
    private PreparedStatement addPermissionStatement;
    private PreparedStatement linkRoleStatement;
    private PreparedStatement linkPermissionStatement;
    private PreparedStatement getUserRoleStatement;
    private PreparedStatement getRolePermissionsStatement;

	private Statement statement;

}
