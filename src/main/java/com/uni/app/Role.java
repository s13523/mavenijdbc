package com.uni.app;

import java.util.ArrayList;
import java.util.List;

public class Role{
    public Role(
        String name,
        List<String> permissions
    ){
        this.name = name;
        if( permissions == null )
            this.permissions = new ArrayList<String>();
        else
            this.permissions = permissions;
    }


    String name;
    public String getName(){
        return name;
    }
    
    public void setName( String name ){
        this.name = name;
    }


    List<String> permissions;
    public List<String> getPermissions(){
        return permissions;
    }
    
    public void setPermissions( List<String> permissions ){
        this.permissions = permissions;
    }

    public Role withPermission( String permission ){
        permissions.add(permission);
        return this;
    }
}
