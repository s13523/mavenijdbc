package com.uni.app;

import java.util.ArrayList;
import java.util.List;

public class User{
    public User(
        String login,
        String pass,
        String phone,
        List<String> addresses,
        List<Role> roles
    ){
        this.login = login;
        this.pass = pass;
        this.phone = phone;

        if( addresses == null )
            this.addresses = new ArrayList<String>();
        else
            this.addresses = addresses;

        if( roles == null )
            this.roles = new ArrayList<Role>();
        else
            this.roles = roles;
    }


    String login;
    public String getLogin(){
        return login;
    }
    
    public void setLogin( String login ){
        this.login = login;
    }


    String pass;
    public String getPass(){
        return pass;
    }
    
    public void setPass( String pass ){
        this.pass = pass;
    }


    String phone;
    public String getPhone(){
        return phone;
    }
    
    public void setPhone( String phone ){
        this.phone = phone;
    }


    List<String> addresses;
    public List<String> getAddresses(){
        return addresses;
    }
    
    public void setAddresses( List<String> addresses ){
        this.addresses = addresses;
    }

    public User withAddress( String address ){
        addresses.add( address );
        return this;
    }


    List<Role> roles;
    public List<Role> getRoles(){
        return roles;
    }
    
    public void setRoles( List<Role> roles ){
        this.roles = roles;
    }

    public User withRole( Role role ){
        roles.add(role);
        return this;
    }
}
