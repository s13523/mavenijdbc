package com.uni.app;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;

public class AppTest 
    extends TestCase
{
    public AppTest( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testApp()
    {
        assert(true);
    }

    public void testRoles(){
        Role role = new Role( "admin", null );
        role.withPermission( "write" );
        role.withPermission( "read" );
        assert(role.getName().equals("admin"));
        assert(role.getPermissions().size() == 2);
    }

    public void testUser(){
        stdUserTest( makeStdUser() );
    }

    public void testUserDB(){
        UserDB db = new UserDB();
        db.clearUsers();
        assert(db.getUsers().size() == 0);
        db.addUser(makeStdUser());
        assert(db.getUsers().size() == 1);
        stdUserTest( db.getUsers().get(0) );
    }

    public void testUserDBAddress(){
        UserDB db = new UserDB();
        db.clearUsers();
        db.addUser(
            makeStdUser().withAddress("foo")
                         .withRole( new Role( "user", null )
                            .withPermission( "read" )
                         )
        );
        assert(db.getUsers().size() == 1);
        User user = db.getUsers().get(0);
        List<String> addresses = user.getAddresses();
        assert( addresses.size() == 1 );
        assert( addresses.get(0).equals( "foo" ) );
        List<Role> roles = user.getRoles();
        assert( roles.size() == 1 );
        Role role = roles.get(0);
        assert( role.getName().equals("user") );
        assert( role.getPermissions().size() == 1 );
        assert( role.getPermissions().get(0).equals("read"));
    }

    private void stdUserTest( User user ){
        assert(user.getLogin().equals("some string value"));
        assert(user.getPass().equals("some string value"));
        assert(user.getPhone().equals("some string value"));
        assert(user.getAddresses().size() == 0);
    }

    private User makeStdUser(){
        return new User(
            "some string value", 
            "some string value", 
            "some string value", 
            null, 
            null
        );
    }
}
